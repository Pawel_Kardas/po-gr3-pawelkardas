package pl.edu.uwm.wmii.pawelkardas.laboratorium01;


import java.util.LinkedList;
import java.util.ListIterator;


public class zad2 {

    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        redukuj(list, -2);

        System.out.println(list);
    }

    public static void redukuj(LinkedList<String> komputery, int n) {
        if (n < 1) {
            return;
        }
        int licznikElementow = 1;
        ListIterator<String> iterator = komputery.listIterator();
        while (iterator.hasNext()) {
            iterator.next();
            if (licznikElementow % n == 0)
                iterator.remove();
            licznikElementow++;
        }
    }
}
