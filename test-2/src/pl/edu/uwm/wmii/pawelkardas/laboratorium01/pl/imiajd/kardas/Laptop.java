package pl.edu.uwm.wmii.pawelkardas.laboratorium01.pl.imiajd.kardas;

import java.time.LocalDate;

public class Laptop extends Komputer implements Cloneable, Comparable<Komputer> {

    private boolean czyApple;

    public Laptop(String nazwa, LocalDate dataProdukcji, boolean czyApple) {
        super(nazwa, dataProdukcji);
        this.czyApple = czyApple;
    }


    @Override
    public int compareTo(Komputer jd) {
        if (super.compareTo(jd) == 0) {
            Laptop laptop = (Laptop) jd;
            return Boolean.compare(czyApple, laptop.czyApple);
        }
        return super.compareTo(jd);
    }

    @Override
    public String toString() {
        return "Laptop: " + "nazwa: " + nazwa + ", data produkcji: " + dataProdukcji + ", czy apple ? = " + czyApple;
    }
}






