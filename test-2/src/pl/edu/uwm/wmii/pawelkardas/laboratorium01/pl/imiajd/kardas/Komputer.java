package pl.edu.uwm.wmii.pawelkardas.laboratorium01.pl.imiajd.kardas;


import java.time.LocalDate;


public class Komputer implements Cloneable, Comparable<Komputer> {
    public Komputer(String nazwa, LocalDate dataProdukcji) {
        this.nazwa = nazwa;
        this.dataProdukcji = dataProdukcji;
    }


    @Override
    public boolean equals(Object oBj) {
        if (this == oBj) return true;
        if (oBj == null || getClass() != oBj.getClass()) return false;
        Komputer komputer = (Komputer) oBj;
        return nazwa.equals(komputer.nazwa) && dataProdukcji.equals(komputer.dataProdukcji);
    }

    @Override
    public int compareTo(Komputer z) {

        if (nazwa.charAt(0) > z.nazwa.charAt(0)) {
            return 1;
        } else if (nazwa.charAt(0) < z.nazwa.charAt(0)) {
            return -1;
        } else {
            if (dataProdukcji.isBefore(z.dataProdukcji)) {
                return -1;
            } else if (!dataProdukcji.isBefore(z.dataProdukcji)) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    @Override
    protected Komputer clone() throws CloneNotSupportedException {
        Komputer cloned = (Komputer) super.clone();
        return cloned;
    }

    @Override
    public String toString() {
        return "Nazwa[" + nazwa + ", " + dataProdukcji +
                ']';
    }

    protected final String nazwa;
    protected final LocalDate dataProdukcji;

}
