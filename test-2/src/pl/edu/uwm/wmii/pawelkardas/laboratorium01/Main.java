package pl.edu.uwm.wmii.pawelkardas.laboratorium01;


import pl.edu.uwm.wmii.pawelkardas.laboratorium01.pl.imiajd.kardas.Komputer;
import pl.edu.uwm.wmii.pawelkardas.laboratorium01.pl.imiajd.kardas.Laptop;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Komputer> grupa = new ArrayList<>();
        grupa.add(new Komputer("Vaio", LocalDate.of(1999,2,5)));
        grupa.add(new Komputer("Vaio", LocalDate.of(1999,4,12)));
        grupa.add(new Komputer("Thinkpad", LocalDate.of(1993,1,3)));
        grupa.add(new Komputer("Latitude", LocalDate.of(1993,1,3)));
        grupa.add(new Komputer("Macbook air", LocalDate.of(1992,10,1)));

        System.out.println(grupa);
        Collections.sort(grupa);
        System.out.println(grupa);


        List<Laptop> grupaLaptopow = new ArrayList<>();
        grupaLaptopow.add(new Laptop("Lenovo", LocalDate.of(1999,5,1),false));
        grupaLaptopow.add(new Laptop("Lenovo", LocalDate.of(1992,1,5),false));
        grupaLaptopow.add(new Laptop("Apple", LocalDate.of(1996,3,7),true));
        grupaLaptopow.add(new Laptop("Dell", LocalDate.of(1992,2,1),false));
        grupaLaptopow.add(new Laptop("Samsung", LocalDate.of(1996,3,7),false));

        System.out.println(grupaLaptopow);
        Collections.sort(grupaLaptopow);
        System.out.println(grupaLaptopow);
    }

}
