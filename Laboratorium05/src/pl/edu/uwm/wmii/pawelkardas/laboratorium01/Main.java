package pl.edu.uwm.wmii.pawelkardas.laboratorium01;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;


public class Main {

    public static void main(String[] args) {
    }
}
class Zadanie1 {
    public static void main(String[]args)
    {
        ArrayList<Integer> a = new ArrayList();
        ArrayList<Integer> b = new ArrayList();
        a.add(1);
        a.add(2);
        b.add(3);
        b.add(4);
        ArrayList test = append(a,b);
        System.out.println(test);
    }
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> wynik = new ArrayList<>();
        wynik.addAll(a);
        wynik.addAll(b);
        return wynik;
    }
}
class Zadanie2 {
    public static void main(String[]args)
    {
        ArrayList<Integer> a = new ArrayList();
        ArrayList<Integer> b = new ArrayList();
        a.add(1);
        a.add(3);
        a.add(5);
        a.add(7);

        b.add(2);
        b.add(4);
        b.add(4);
        b.add(6);
        b.add(8);
        b.add(8);
        b.add(8);

        ArrayList test = merge(a,b);
        System.out.println(test);
    }
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> wynik = new ArrayList<>();
        int i=0;
        int j=0;
        while(wynik.size()<a.size()+b.size())
        {
            if(i<a.size())
            {
                wynik.add(a.get(i));
                i++;
            }
            if(j<b.size())
            {
                wynik.add(b.get(j));
                j++;
            }
        }
        return wynik;
    }
}
class Zadanie3 {
    public static void main(String []args)
    {
        ArrayList<Integer> a = new ArrayList();
        ArrayList<Integer> b = new ArrayList();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        b.add(4);
        b.add(7);
        b.add(9);
        b.add(9);
        b.add(11);
        b.add(22);
        b.add(33);
        ArrayList test = mergeSorted(a,b);
        System.out.println(test);


    }
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> wynik = new ArrayList<>();


        int i = 0;
        int j = 0;
        while (wynik.size() < a.size() + b.size()) {
            {
                if (i == a.size()) {
                    wynik.add(b.get(j));
                    j++;
                    continue;
                } else if (j == b.size()) {
                    wynik.add(a.get(i));
                    i++;
                    continue;
                }
                if (a.get(i) <= b.get(j)) {
                    wynik.add(a.get(i));
                    i++;
                } else {
                    wynik.add(b.get(j));
                    j++;
                }

            }

        }
        return wynik;
    }
}
class Zadanie4 {
    public static void main(String[]args)
    {
        ArrayList <Integer>a = new ArrayList();
        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        ArrayList test = reversed(a);
        System.out.println(test);
    }
    public static ArrayList<Integer> reversed(ArrayList<Integer> a)
    {
        ArrayList wynik = new ArrayList();
        for(int i=a.size()-1;i>=0;i--)
        {
            wynik.add(a.get(i));
        }
        return wynik;
    }

}
class Zad5 {
    public static void main(String[]args)
    {
        ArrayList<Integer> a = new ArrayList();
        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        reverse(a);
        System.out.println(a);
    }
    public static void reverse(ArrayList<Integer> a)
    {
        ArrayList pom = (ArrayList) a.clone();
        int j=0;
        for(int i=a.size()-1;i>=0;i--)
        {
            int tmp = (int) pom.get(i);
            a.set(j,tmp);
            j++;

        }

    }
}




