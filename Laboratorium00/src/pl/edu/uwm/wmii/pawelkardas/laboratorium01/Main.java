package pl.edu.uwm.wmii.pawelkardas.laboratorium00;

public class Main {
    public static void main(String[] arg) {
    }
}

class Zadanie1 {
    public static void main(String[] args) {
        System.out.println(31 + 29 + 31);
    }
}

class Zadanie2 {
    public static void main(String[] args) {
        System.out.println(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10);
    }
}

class Zadanie3 {
    public static void main(String[] arg) {
        System.out.println(2 * 3 * 4 * 5 * 6 * 7 * 8 * 9 * 10);
    }
}

class Zadanie4 {
    public static void main(String[] arg) {
        System.out.println(1000);
        System.out.println(1000 + (1000 * 6 / 100));
        System.out.println(1060 + (1060 * 6 / 100));
    }
}

class Zadanie5 {
    public static void main(String[] args) {
        String name = "JAVA\n";
        System.out.println(name);
    }
}

class Zadanie6 {
    public static void main(String[] arg) {
        String name = """
                                
                   /////     
                  +----+    
                (|  o   o  |)
                  |  ^  |     
                 |  <_>  |    
                 +-------+    
                       """;
        System.out.println(name);
    }
}

class Zadanie7 {
    public static void main(String[] args) {
        String name = """
                  ************  *       *
                  *          *  *     *
                  *          *  *  *
                  ************  **
                  *             *  *
                  *             *     *
                  *             *       *
                """;
        System.out.println(name);
    }
}

class Zadanie8 {
    public static void main(String[] args) {
        String name = """
                       +
                      + +
                     +   +
                    +-----+
                    | .-. |
                    | | | |
                    +-+-+-+
                """;
        System.out.println(name);
    }
}

class Zadanie9 {
    public static void main(String[] args) {
        String name = """
                    ++_++       +---+
                   ( 00  )     +     +
                 ==(  -  )==  + BLESS +
                    | | |     + YOU!  +
                    | | |      +     +
                    | | |       +___+
                    (-)(-)
                """;
        System.out.println(name);
    }
}

class Zadanie10 {
    public static void main(String[] args) {
        String name = "Robert";
        String name2 = "Jan";
        String name3 = "Pawel";
        System.out.println(name);
        System.out.println(name2);
        System.out.println(name3);
    }
}

class Zadanie11 {
    public static void main(String[] args) {
        String name = """
                Co innego cebula.
                Ona nie ma wnętrzności.
                Jest sobą na wskroś cebula
                do stopnia cebuliczności.
                Cebulasta na zewnątrz,
                cebulowa do rdzenia,
                mogłaby wejrzeć w siebie
                cebula bez przerażenia.
                """;
        System.out.println(name);
    }
}

class Zadanie12 {
    public static void main(String[] args) {
        String name = """
                * * * * * * * ==================
                 * * * * * *  ==================
                * * * * * * * ==================
                 * * * * * *  ==================
                ================================
                ================================
                ================================
                """;
        System.out.println(name);
    }
}
