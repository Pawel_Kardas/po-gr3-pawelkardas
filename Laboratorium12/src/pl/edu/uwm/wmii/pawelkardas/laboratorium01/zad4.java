package pl.edu.uwm.wmii.pawelkardas.laboratorium01;


import java.util.ListIterator;

public class zad4 {
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        System.out.println(list);
        odwroc(list);
        System.out.println(list);
        System.out.println();
        LinkedList<Integer> list2 = new LinkedList<Integer>();
        list2.add(1);
        list2.add(2);
        list2.add(3);
        list2.add(4);
        list2.add(5);
        list2.add(6);
        list2.add(7);
        list2.add(8);
        System.out.println(list2);
        odwroc(list2);
        System.out.println(list2);

    }

    public static <T> void odwroc(LinkedList<T> lista) {
        ListIterator list_IterBeg = lista.listIterator();
        ListIterator list_IterEnd = lista.listIterator(lista.size());

        for (int i = 0; i < lista.size() / 2; i++) {
            T pom = (T) list_IterEnd.previous();
            list_IterEnd.set(list_IterBeg.next());
            list_IterBeg.set(pom);


        }

    }
}
