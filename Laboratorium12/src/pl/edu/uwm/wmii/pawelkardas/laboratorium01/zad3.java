package pl.edu.uwm.wmii.pawelkardas.laboratorium01;

import java.util.LinkedList;
import java.util.ListIterator;



public class zad3 {
    public static void main (String[]args)
    {
        LinkedList<String> list = new LinkedList<String>();
        list.add("Nowak1");
        list.add("Nowak2");
        list.add("Nowak3");
        list.add("Nowak4");
        list.add("Nowak5");
        list.add("Nowak6");
        System.out.println(list);
        odwroc(list);
        System.out.println(list);
    }
    public static void odwroc(LinkedList<String> lista)
    {
        ListIterator list_IterBeg = lista.listIterator();
        ListIterator list_IterEnd = lista.listIterator(lista.size());

        for(int i=0;i<lista.size()/2;i++)
        {
            String pom =  list_IterEnd.previous().toString();
            list_IterEnd.set(list_IterBeg.next());
            list_IterBeg.set(pom);
        }
    }
}