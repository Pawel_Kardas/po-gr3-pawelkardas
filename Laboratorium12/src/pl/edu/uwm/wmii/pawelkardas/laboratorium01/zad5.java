package pl.edu.uwm.wmii.pawelkardas.laboratorium01;

import java.util.Stack;

public class zad5 {
    public static void main(String[] args) {
        Stack<Character> pom = new Stack<>();
        pom.add('a');
        pom.add('b');
        pom.add('c');
        String a = "Ala ma kota. Jej kot lubi myszy.";
        wypisz(a);
    }

    public static void wypisz(String slowo) {
        Stack<String> stack = new Stack<>();
        StringBuilder pom = new StringBuilder();
        for (char c : slowo.toCharArray()) {
            if(c!=' ' && c!='.')
            {
                pom.append(c);
            }
            else if(c==' ')
            {
                if(pom.length()!=0) {
                    stack.add(pom.toString());
                }
                pom=new StringBuilder();
            }
            else if( c=='.')
            {
                stack.add(pom.toString());
                pom=new StringBuilder();
                int i =1;
                int j = stack.size();
                while(!stack.empty())
                {   String tmp = stack.pop();

                    if(i==1)
                    {
                        System.out.print(Character.toUpperCase(tmp.charAt(0))+tmp.substring(1)+' ');
                    }
                    else if(i==j)
                    {
                        System.out.print(Character.toLowerCase(tmp.charAt(0))+tmp.substring(1)+". ");
                    }
                    else
                    {
                        System.out.print(tmp+" ");
                    }
                    i++;
                }
            }
        }
    }
}