package pl.edu.uwm.wmii.pawelkardas.laboratorium01;


class Zad1 implements Comparable<Zad1> {
    private int priority;
    private String description;

    public Zad1(int priority, String description) {
        this.priority = priority;
        this.description = description;
    }
    @Override
    public String toString() {
        return priority+" "+description;
    }
    @Override
    public int compareTo(Zad1 o) {
        return Integer.compare(priority,o.priority);
    }
}
