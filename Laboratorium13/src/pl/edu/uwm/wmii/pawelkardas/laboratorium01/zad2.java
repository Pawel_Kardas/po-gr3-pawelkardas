package pl.edu.uwm.wmii.pawelkardas.laboratorium01;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class zad2 {
    public static void main(String[] args) {
        Map<String,String> tab = new HashMap();
        boolean rrs = true;
        Scanner scanner = new Scanner(System.in);
        while(rrs){
            System.out.println("1. Dodaj studenta");
            System.out.println("2. Usuń studenta");
            System.out.println("3. Zmień ocene");
            System.out.println("4. Wypisz studentów");
            System.out.println("5. Zakończ");
            int s = scanner.nextInt();
            if(s==1){
                System.out.println("Imie");
                String imie = scanner.next();
                System.out.println("Ocena");
                String ocena = scanner.next();
                tab.put(imie,ocena);
            }
            else if(s==2){
                System.out.println("Imie student do usuniecia");
                tab.remove(scanner.next());
            }
            else if(s==3){
                System.out.println("Imie");
                String imie = scanner.next();
                System.out.println("Ocena");
                String ocena = scanner.next();
                tab.replace(imie,ocena);
            }
            else if(s==4){
                Map<String, String> sort = new TreeMap<>(tab);
                for (String str : sort.keySet()) {
                    System.out.println(str+" : "+sort.get(str));

                }
                System.out.println();
            }
            else if(s==5){
                rrs=false;
            }
        }
    }
}
