package pl.edu.uwm.wmii.pawelkardas.laboratorium01;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

    }
}

class Zadanie1_a {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj liczbe n: ");
        int n = scanner.nextInt();
        int[] tab = new int[n];
        int start= -999;
        int end= 999;
        int liczba;
        Random random = new Random();
        for(int q=0;q<n;q++) {
            liczba = random.nextInt(end-start+1) +start;
            tab[q] = liczba;
            System.out.print(tab[q] +" | ");
        }//generator liczb

        int par = 0;
        int niepar = 0;
        for(int i=0;i<n;i++){
            if(tab[i]%2==0){
                par = par +1;
            }
            else{
                niepar = niepar +1;
            }
        }
        System.out.print("\nilosc parzystych liczb: "+par+"\nilosc nieparzystych liczb: "+niepar);
    }
}

class Zadanie1_b {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj liczbe n: ");
        int n = scanner.nextInt();
        int[] tab = new int[n];
        int start= -999;
        int end= 999;
        int liczba;
        Random random = new Random();
        for(int q=0;q<n;q++) {
            liczba = random.nextInt(end-start+1) +start;
            tab[q] = liczba;
            System.out.print(tab[q] +" | ");
        }//generator liczb

        int dodatnie = 0;
        int ujemne = 0;
        int zera = 0;
        for(int i=0;i<n;i++){
            if(tab[i] >=0){
                dodatnie = dodatnie +1;
            }
            if(tab[i] < 0){
                ujemne = ujemne+1;
            }
            if(tab[i] == 0){
                zera = zera +1;
            }
        }
        System.out.print("\nilosc liczb:\ndodatnie: "+dodatnie+"\nujemne: "+ujemne+"\nzera: "+zera);
    }
}


class Zadanie1_c {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj liczbe n: ");
        int n = scanner.nextInt();
        int[] tab = new int[n];
        int start= -999;
        int end= 999;
        int liczba;
        Random random = new Random();
        for(int q=0;q<n;q++) {
            liczba = random.nextInt(end-start+1) +start;
            tab[q] = liczba;
            System.out.print(tab[q] +" | ");
        }//generator liczb

        int max = tab[0];
        for(int i=0;i<n;i++){
            if(tab[i] > max){
                max = tab[i];
            }
        }
        int ilemax = 0;
        for(int j=0;j<n;j++){
            if(tab[j] == max){
                ilemax = ilemax +1;
            }
        }
        System.out.print("\nmax tablicy to: " + max + "\nilosc max to: "+ilemax);
    }
}

class Zadanie1_d {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj liczbe n: ");
        int n = scanner.nextInt();
        int[] tab = new int[n];
        int start= -999;
        int end= 999;
        int liczba;
        Random random = new Random();
        for(int q=0;q<n;q++) {
            liczba = random.nextInt(end-start+1) +start;
            tab[q] = liczba;
            System.out.print(tab[q] +" | ");
        }//generator liczb

        int sumadod=0;
        int sumauj=0;
        for(int i=0;i<n;i++){
            if(tab[i] >=0){
                sumadod = sumadod + tab[i];
            }
            else{
                sumauj = sumauj + tab[i];
            }
        }
        System.out.print("\nsuma dodatnich liczb: "+sumadod+"\nsuma ujemnych liczb: "+sumauj);
    }
}


class Zadanie1_f {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj liczbe n: ");
        int n = scanner.nextInt();
        int[] tab = new int[n];
        int start= -999;
        int end= 999;
        int liczba;
        Random random = new Random();
        for(int q=0;q<n;q++) {
            liczba = random.nextInt(end-start+1) +start;
            tab[q] = liczba;
            System.out.print(tab[q] +" | ");
        }//generator liczb

        System.out.print("\n");
        for(int i=0;i<n;i++){
            if(tab[i] >=0){
                tab[i] = 1;
            }
            else{
                tab[i] = -1;
            }
            System.out.print(tab[i] + " | ");
        }
    }
}

class Zadanie2_a {

    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc){
        int start= minWartosc;
        int end= maxWartosc;
        int liczba;
        Random random = new Random();
        for(int i=0;i<n;i++) {
            liczba = random.nextInt(end-start+1) +start;
            tab[i] = liczba;
        }
    }

    public static void wypisz(int tab[], int n){
        for(int i=0;i<n;i++){
            System.out.print(tab[i]+" | ");
        }
    }

    public static int ileNieparzystych(int tab[]){
        int niepar = 0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]%2!=0){

                niepar = niepar +1;
            }
        }
        return niepar;
    }

    public static int ileParzystych(int tab[]){
        int par = 0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]%2==0){

                par = par +1;
            }
        }
        return par;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj liczbe n: ");
        int n = scanner.nextInt();
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        wypisz(tab, n);
        System.out.print("\nilosc parzystych: "+ileParzystych(tab));
        System.out.print("\nilosc nieparzystych: "+ileNieparzystych(tab));

    }
}

 class Zadanie2_b {

    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc){
        int start= minWartosc;
        int end= maxWartosc;
        int liczba;
        Random random = new Random();
        for(int i=0;i<n;i++) {
            liczba = random.nextInt(end-start+1) +start;
            tab[i] = liczba;
        }
    }

    public static void wypisz(int tab[], int n){
        for(int i=0;i<n;i++){
            System.out.print(tab[i]+" | ");
        }
    }

    public static int ileDodatnich(int tab[]){
        int wynik = 0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]>=0){
                wynik = wynik +1;
            }
        }
        return wynik;
    }

    public static int ileUjemnych(int tab[]){
        int wynik = 0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]<0){
                wynik = wynik +1;
            }
        }
        return wynik;
    }

    public static int ileZerowych(int tab[]){
        int wynik = 0;
        for(int i=0;i<tab.length;i++){
            if(tab[i] == 0){
                wynik = wynik +1;
            }
        }
        return  wynik;
    }


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj liczbe n: ");
        int n = scanner.nextInt();
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        wypisz(tab, n);
        System.out.print("\nilosc dodatnich: "+ ileDodatnich(tab)+"\nilosc ujemnych: "+ileUjemnych(tab) + "\nilosc zer: "+ileZerowych(tab));

    }
}

class Zadanie2_c {

    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc){
        int start= minWartosc;
        int end= maxWartosc;
        int liczba;
        Random random = new Random();
        for(int i=0;i<n;i++) {
            liczba = random.nextInt(end-start+1) +start;
            tab[i] = liczba;
        }
    }

    public static void wypisz(int tab[], int n){
        for(int i=0;i<n;i++){
            System.out.print(tab[i]+" | ");
        }
    }

    public static int ileMaksymalny(int tab[]){
        int max = tab[0];
        for(int i=0;i<tab.length;i++){
            if(tab[i] > max){
                max = tab[i];
            }
        }
        int ilosc = 0;
        for(int j=0;j<tab.length;j++){
            if(tab[j] == max){
                ilosc = ilosc +1;
            }
        }
        return ilosc;
    }


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj liczbe n: ");
        int n = scanner.nextInt();
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        wypisz(tab, n);
        System.out.print("\nilosc max: "+ ileMaksymalny(tab));

    }
}

class Zadanie2_d {

    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc){
        int start= minWartosc;
        int end= maxWartosc;
        int liczba;
        Random random = new Random();
        for(int i=0;i<n;i++) {
            liczba = random.nextInt(end-start+1) +start;
            tab[i] = liczba;
        }
    }

    public static void wypisz(int tab[], int n){
        for(int i=0;i<n;i++){
            System.out.print(tab[i]+" | ");
        }
    }

    public static int sumaDodatnia(int tab[]){
        int wynik = 0;
        for(int i=0;i<tab.length;i++){
            if(tab[i] >=0){
                wynik = wynik +tab[i];
            }
        }
        return wynik;
    }

    public static int sumaUjemnych(int tab[]){
        int wynik = 0;
        for(int i=0;i<tab.length;i++){
            if(tab[i] <0){
                wynik = wynik +tab[i];
            }
        }
        return wynik;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj liczbe n: ");
        int n = scanner.nextInt();
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        wypisz(tab, n);
        System.out.print("\nsuma dodatnich liczb: "+sumaDodatnia(tab));
        System.out.print("\nsuma ujemnych liczb: "+sumaUjemnych(tab));

    }
}
class Zadanie2_f {

    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc){
        int start= minWartosc;
        int end= maxWartosc;
        int liczba;
        Random random = new Random();
        for(int i=0;i<n;i++) {
            liczba = random.nextInt(end-start+1) +start;
            tab[i] = liczba;
        }
    }

    public static void wypisz(int tab[], int n){
        for(int i=0;i<n;i++){
            System.out.print(tab[i]+" | ");
        }
    }

    public static void signum(int tab[]){
        System.out.print("\n");
        for(int i=0;i<tab.length;i++){
            if(tab[i] >=0){
                tab[i] = 1;
            }
            else{
                tab[i] = -1;
            }
            System.out.print(tab[i] + " | ");
        }
    }


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj liczbe n: ");
        int n = scanner.nextInt();
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        wypisz(tab, n);
        signum(tab);

    }
}