package pl.edu.uwm.wmii.pawelkardas.laboratorium01;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.math.BigDecimal;
import java.math.BigInteger;

public class Main {

    public static void main(String[] args) {
    }
}

class Zadanie1a {
    public static void main(String[]args)
    {

        System.out.println(countChar("cocacola",'c'));
    }
    public static int countChar(String str,char c )
    {   int wynik=0;
        for(char i:str.toCharArray())
        {
            if(i==c)
            {
                wynik++;
            }
        }
        return wynik;
    }
}
class Zadanie1b {
    public static void main(String []args)
    {

        System.out.println(countSubStr("qwertypawelqweqert","pawel"));
    }
    public static int countSubStr(String str, String subStr)
    {   int j=0;
        int wynik=0;
        for(int i=0;i<str.length();i++)
        {
            if(str.charAt(i)==subStr.charAt(j))
            {
                j++;
            }
            else
            {
                j=0;
            }
            if(j==subStr.length()-1)
            {
                wynik++;
                j=0;
            }
        }
        return wynik;
    }
}
class Zadanie1c {
    public static void main(String[]args)
    {
        String str ="siema";

        System.out.println(middle(str));
        System.out.println(middle("sseerr"));

    }
    public static String middle(String str)
    {   String wynik="";


        if(str.length()%2!=0)
        {
            wynik+=str.charAt((str.length()-1)/2);
        }
        else if(str.length()%2==0)
        {   int srod=str.length()/2;

            wynik+=(str.charAt(srod-1));
            wynik+=str.charAt(srod);

        }
        return wynik;
    }
}
class Zadanie1d {
    public static void main(String[]args)
    {

        System.out.println(repeat("ho",3));
    }
    public static String repeat(String str,int n)
    {
        String wynik="";
        for(int i=0;i<n;i++)
        {
            wynik+=str;
        }
        return wynik;
    }
}
class Zadanie1e {
    public static void main(String[]args)
    {
        int []tab=where("wychylylybyby","byby");
        for(int x:tab)
        {
            System.out.println(x);
        }
    }
    public static int[]where(String str,String subStr)
    {
        int[] wynik=new int[subStr.length()];
        int koniec=0;
        int j=0;
        for(int i=0;i<str.length();i++)
        {
            if(str.charAt(i)==subStr.charAt(j))
            {
                j++;
            }
            else
            {
                j=0;
            }
            if(j==subStr.length())
            {   koniec=i;
                break;
            }
        }
        int q=0;
        for(int i=koniec-subStr.length()+1;i<=koniec;i++)
        {
            wynik[q]=i;
            q++;
        }
        return wynik;
    }
}
class Zadanie1f {
    public static void main(String[]args)
    {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String test = change(str);
        System.out.println(test);
    }
    public static String change(String str)
    {
        StringBuffer tmp =new StringBuffer();
        for(char c:str.toCharArray())
        {
            if(Character.isUpperCase(c))
            {
                tmp.append(Character.toLowerCase(c));
            }
            else if(Character.isLowerCase(c))
            {
                tmp.append(Character.toUpperCase(c));
            }
            else
            {
                tmp.append(c);
            }
        }
        String wynik = tmp.toString();
        return wynik;
    }

}
class Zadanie1g {
    public static void main(String []args)
    {
        System.out.println(nice("5000000000"));
    }
    public static String nice(String str)
    {
        StringBuffer tmp = new StringBuffer();
        int dotrzech=0;
        for(int i=str.length()-1;i>=0;i--)
        {
            tmp.append(str.charAt(i));
            dotrzech++;
            if(dotrzech==3)
            {
                tmp.append("'");
                dotrzech=0;
            }
        }
        String wynik = tmp.reverse().toString();
        return wynik;
    }
}
class Zadanie1h {
    public static void main(String[]args)
    {

        System.out.println(nice("500000",2,'*'));
    }
    public static String nice(String str,int coile,char separator)
    {
        StringBuffer tmp = new StringBuffer();
        int dotrzech=0;
        for(int i=str.length()-1;i>=0;i--)
        {
            tmp.append(str.charAt(i));
            dotrzech++;
            if(dotrzech==coile &&i!=0)
            {
                tmp.append(separator);
                dotrzech=0;
            }
        }
        String wynik = tmp.reverse().toString();
        return wynik;
    }
}
class Zadanie2 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("nazwa pliku");
        String nazwapliku=scanner.next();
        File plik = new File(nazwapliku);
        Scanner in = new Scanner(plik);
        int wynik=0;
        System.out.println("litera");
        String pom=scanner.next();
        while (in.hasNextLine() != false) {
            for(char c:in.nextLine().toCharArray())
            {
                if(c==pom.charAt(0))
                {
                    wynik++;
                }
            }
        }
        System.out.println(wynik);
    }
}
class Zadanie3 {
    public static void main(String[]args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("nazwa pliku");
        String nazwapliku=scanner.next();
        File plik = new File(nazwapliku);
        Scanner in = new Scanner(plik);
        int wynik=0;
        System.out.println("slowo");
        String pom=scanner.next();
        int i=0;
        while (in.hasNextLine() != false) {
            for(char c:in.nextLine().toCharArray())
            {
                if(c==pom.charAt(i))
                {
                    i++;
                }
                else
                {
                    i=0;
                }
                if(i==pom.length())
                {
                    wynik++;
                    i=0;
                }
            }

        }
        System.out.println(wynik);
    }
}
class Zadanie4 {
    public static void main(String[]args)
    {
        BigInteger wynik = foo(10);
        System.out.println(wynik);
    }
    public static BigInteger foo(int n) {
        BigInteger a = BigInteger.valueOf(0);
        BigInteger b = BigInteger.valueOf(1);
        for(int i = 0; i < n * n; i++) {
            a = a.add(b);
            b = b.multiply(BigInteger.valueOf(2));
        }

        return a;
    }
}
class Zadanie5 {
    public static void main(String[]args)
    {
        System.out.println(foo(5, 125250, 4));
    }

    public static BigDecimal foo(int n, int k, int p) {
        BigDecimal wynik = BigDecimal.valueOf(k);

        for(int i = 0; i < n; i++) {
            wynik = wynik.add(wynik.divide(BigDecimal.valueOf(100)).multiply(BigDecimal.valueOf(p)));
        }

        return wynik;
    }

}









