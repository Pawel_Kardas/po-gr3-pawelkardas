package pl.edu.uwm.wmii.pawelkardas.laboratorium01.imajd.kardas;


public class Fortepian extends Instrument {
    public Fortepian(String producent, int year, int month, int day) {
        super(producent, year, month, day);
    }

    @Override
    public String Dzwiek() {
        return "Fortepian: bam, bam, bam";
    }

    @Override
    public String toString() {
        return "Fortepian: "+ super.toString();
    }
}
