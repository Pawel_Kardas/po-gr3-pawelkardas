package pl.edu.uwm.wmii.pawelkardas.laboratorium01.imajd.kardas;

public class Flet extends Instrument {
    public Flet(String producent, int year, int month, int day) {
        super(producent, year, month, day);
    }

    @Override
    public String Dzwiek() {
        return "Flet: fi, fi, fi";
    }

    @Override
    public String toString() {
        return "Flet: "+super.toString();
    }


}
