package pl.edu.uwm.wmii.pawelkardas.laboratorium01;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        System.out.println("a = " + a);

        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);

        System.out.println("b = " + b);

        //Z1
        System.out.println("Append: " + append(a, b));

        //Z2
        System.out.println("Merge: " + merge(a, b));

        //Z3
        System.out.println("MergeSorted: " + mergeSorted(a, b));

        //Z4
        System.out.println("Reversed: " + reversed(a));

        //Z5
        reverse(a);
    }

    //Z1
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> arrL = new ArrayList<>();
        arrL.addAll(0, a);
        arrL.addAll(a.size(), b);
        return arrL;
    }

    //Z2
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> arrL = new ArrayList<>();
        int size = Math.max(a.size(), b.size());
        for (int j = 0; j < size; j++) {
            if (j < a.size()) arrL.add(a.get(j));
            if (j < b.size()) arrL.add(b.get(j));
        }
        return arrL;
    }

    //Z3
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> arrL = new ArrayList<>();

        int i = 0, j = 0;

        while (i < a.size() && j < b.size()) {
            if (a.get(i) < b.get(j)) arrL.add(a.get(i++));
            else arrL.add(b.get(j++));
        }
        while (i < a.size()) arrL.add(a.get(i++));
        while (j < a.size()) arrL.add(a.get(j++));

        return arrL;
    }

    //Z4
    public static ArrayList<Integer> reversed(ArrayList<Integer> a) {
        ArrayList<Integer> arrL = new ArrayList<>();
        for (int j = a.size() - 1; j >= 0; j--) {
            arrL.add(a.get(j));
        }

        return arrL;
    }

    //Z5
    public static void reverse(ArrayList<Integer> a) {
        int x, index;

        for (int i = 0; i < a.size() / 2; i++) {
            index = a.size() - i - 1;
            x = a.get(i);
            a.set(i, a.get(index));
            a.set(index, x);
        }
        System.out.println(a);
    }
