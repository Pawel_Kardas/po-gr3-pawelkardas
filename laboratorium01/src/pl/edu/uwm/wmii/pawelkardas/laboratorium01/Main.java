package pl.edu.uwm.wmii.pawelkardas.laboratorium01;

public class Main {

    public static void main(String[] args) {

    }
}

class Zadanie1A {
    public static void main(String[] args) {
        int suma=0;
        for(int a=0;a<20;a++)
        {
            suma+=a;
        }
        System.out.println(suma);
    }
}

class Zadanie1B {
    public static void main(String[] args) {
        int suma=1;
        for(int a=1;a<20;a++)
        {
            suma*=a;
        }
        System.out.println(suma);
    }
}

class Zadanie1C {

    public static void main(String[] args) {
        int sumaabs=0;
        int[] tab= new int[8];
        tab[0]=1;
        tab[1]=-2;
        tab[2]=3;
        tab[3]=-4;
        tab[4]=6;
        tab[5]=2;
        tab[6]=1;
        tab[7]=0;
        for(int a=0;a<8;a++)
        {
            sumaabs+=Math.abs(tab[a]);//19
        }


        System.out.println(sumaabs);
    }
}

class Zadanie1D {
    public static void main(String[] args) {
        int sumaabs = 0;
        int[] tab = new int[8];
        tab[0] = 1;
        tab[1] = -2;
        tab[2] = 3;
        tab[3] = -4;
        tab[4] = 6;
        tab[5] = 2;
        tab[6] = 1;
        tab[7] = 0;
        for (int a = 0; a < 8; a++) {
            sumaabs += Math.abs(tab[a]);//19
        }


        System.out.println(sumaabs);
    }
}
class Zadanie1E {
    public static void main(String[] args) {
        int abs=1;
        int[] tab= new int[8];
        tab[0]=1;
        tab[1]=-2;
        tab[2]=3;
        tab[3]=-4;
        tab[4]=6;
        tab[5]=2;
        tab[6]=1;
        tab[7]=4;
        for(int a=0;a<8;a++)
        {
            abs*=Math.abs(tab[a]);
        }


        System.out.println(abs);
    }
}

class Zadanie1F {

    public static void main(String[] args) {
        int sumakw=0;
        int[] tab= new int[8];
        tab[0]=1;
        tab[1]=-2;
        tab[2]=3;
        tab[3]=-4;
        tab[4]=6;
        tab[5]=2;
        tab[6]=1;
        tab[7]=0;
        for(int a=0;a<8;a++)
        {
            sumakw+=Math.pow(tab[a],2);
        }


        System.out.println(sumakw);
    }
}

class Zadanie1G {

    public static void main(String[] args) {
        int suma=0;
        int ilo=1;
        int[] tab= new int[8];
        tab[0]=1;
        tab[1]=-2;
        tab[2]=3;
        tab[3]=-4;
        tab[4]=6;
        tab[5]=2;
        tab[6]=1;
        tab[7]=0;
        for(int a=0;a<8;a++)
        {
            suma+=(tab[a]);
            ilo*=tab[a];
        }


        System.out.println("Suma: "+ suma +" Iloczyn: "+ ilo);
    }
}

class Zadanie1H {
    public static void main(String[] args) {
        int wynik=0;
        int[] tab= new int[8];
        tab[0]=1;
        tab[1]=-2;
        tab[2]=3;
        tab[3]=-4;
        tab[4]=6;
        tab[5]=2;
        tab[6]=1;
        tab[7]=0;
        for(int a=0;a<8;a++)
        {
            wynik+=Math.pow(-1,a+2)*tab[a];
        }


        System.out.println(wynik);
    }
}

class Zadanie1I {

    public static void main(String[] args) {
        int wynik=0;
        int[] tab= new int[8];
        tab[0]=1;
        tab[1]=-2;
        tab[2]=3;
        tab[3]=-4;
        tab[4]=6;
        tab[5]=2;
        tab[6]=1;
        tab[7]=0;
        for(int a=0;a<8;a++)
        {
            wynik+=(Math.pow(-1,a+2)*tab[a])/silnia(a+1);
        }


        System.out.println(wynik);
    }
    static int silnia(int n) {
        int iloczyn = 1;
        for (int i=1; i<=n; i++) {
            iloczyn *= i;
        }
        return iloczyn;
    }
}

class Zadanie2_1a {
    public static void main(String[] args) {
        int wynik=0;
        int[] tab= new int[8];
        tab[0]=1;
        tab[1]=-2;
        tab[2]=3;
        tab[3]=-4;
        tab[4]=6;
        tab[5]=2;
        tab[6]=1;
        tab[7]=0;
        for(int a=0;a<8;a++)
        {
            if(tab[a]%2==1) {
                System.out.print(tab[a]+",");
                wynik++;
            }
        }


        System.out.println("\nTakich liczbe jest : "+wynik);
    }
}

class Zadanie2_1b {
    public static void main(String[] args) {
        int wynik=0;
        int[] tab= new int[8];
        tab[0]=1;
        tab[1]=-2;
        tab[2]=3;
        tab[3]=-4;
        tab[4]=15;
        tab[5]=5;
        tab[6]=25;
        tab[7]=6;
        for(int a=0;a<8;a++)
        {
            if(tab[a]%3==0&&tab[a]%5!=0) {
                System.out.print(tab[a]+",");
                wynik++;
            }
        }


        System.out.println("\nTakich liczbe jest : "+wynik);
    }
}

class Zadanie2_1c{
    public static void main(String[] args)
    {
        int wynik=0;
        int[] tab= new int[8];
        tab[0]=1;
        tab[1]=-2;
        tab[2]=4;
        tab[3]=-4;
        tab[4]=6;
        tab[5]=16;
        tab[6]=1;
        tab[7]=36;
        for(int a=0;a<8;a++)
        {
            if(Math.sqrt(tab[a])%2==0) {
                System.out.print(tab[a]+",");
                wynik++;
            }
        }


        System.out.println("\nTakich liczbe jest : "+wynik);
    }
}

class Zadanie2_1d{
    public static void main(String[] args)
    {
        int n=8;
        int wynik=0;
        double[] tab= new double[8];
        tab[0]=1;
        tab[1]=-2;
        tab[2]=7;
        tab[3]=-4;
        tab[4]=2;
        tab[5]=9;
        tab[6]=1;
        tab[7]=4;
        for(int a=1;a<n-1;a++)
        {
            if(tab[a]<(tab[a+1]+tab[a-1])/2)
            {
                System.out.print(tab[a]+",");
                wynik++;
            }
        }


        System.out.println("\nTakich liczbe jest : "+wynik);
    }
}

class Zadanie2_1e{
    public static void main(String[] args)
    {
        int n=8;
        int wynik=0;
        int[] tab= new int[8];
        tab[0]=1;
        tab[1]=3;
        tab[2]=5;
        tab[3]=3;
        tab[4]=3;
        tab[5]=40;
        tab[6]=70;
        tab[7]=4;
        for(int a=1;a<n;a++)
        {
            if(Math.pow(2,a)<tab[a]&&tab[a]<silnia(a))
            {
                System.out.print(tab[a]+",");
                wynik++;
            }
        }


        System.out.println("\nTakich liczbe jest : "+wynik);
    }

    static int silnia(int n)
    {
        int iloczyn = 1;
        for (int i=1; i<=n; i++)
        {
            iloczyn *= i;
        }
        return iloczyn;
    }
}

class Zadanie2_1f {

    public static void main(String[] args) {
        int n = 8;
        int wynik = 0;
        int[] tab = new int[8];
        tab[0] = 2;
        tab[1] = 3;
        tab[2] = 4;
        tab[3] = 3;
        tab[4] = 5;
        tab[5] = 6;
        tab[6] = 8;
        tab[7] = 4;
        for (int a = 0; a < n; a++) {
            if ((a + 1 )% 2 == 1 &&(tab[a]) % 2 == 0) {
                System.out.print(tab[a] + ",");
                wynik++;
            }
        }


        System.out.println("\nTakich liczbe jest : " + wynik);
    }
}

class Zadanie2_1g{

    public static void main(String[] args) {
        int n = 8;
        int wynik = 0;
        int[] tab = new int[8];
        tab[0] = 2;
        tab[1] = 7;
        tab[2] = 1;
        tab[3] = -3;
        tab[4] = 3;
        tab[5] = 6;
        tab[6] = -1;
        tab[7] = -9;
        for (int a = 0; a < n; a++) {
            if (tab[a]>0 &&(tab[a]) % 2 == 1) {
                System.out.print(tab[a] + ",");
                wynik++;
            }
        }


        System.out.println("\nTakich liczbe jest : " + wynik);
    }
}

class Zadanie2_1h{
    public static void main(String[] args) {
        int n = 8;
        int wynik = 0;
        int[] tab = new int[8];
        tab[0] = 2;
        tab[1] = 7;
        tab[2] = 1;
        tab[3] = -3;
        tab[4] = 3;
        tab[5] = 6;
        tab[6] = -1;
        tab[7] = -9;
        for (int a = 0; a < n; a++) {
            if (Math.abs(tab[a])<Math.pow(a,2)) {
                System.out.print(tab[a] + ",");
                wynik++;
            }
        }


        System.out.println("\nTakich liczbe jest : " + wynik);
    }
}

class Zadanie2_2{
    public static void main(String[] args) {
        int n = 8;
        int wynik = 0;
        int[] tab = new int[8];
        tab[0] = 2;
        tab[1] = 7;
        tab[2] = 1;
        tab[3] = -3;
        tab[4] = 3;
        tab[5] = 6;
        tab[6] = -1;
        tab[7] = -9;
        System.out.print("Dodatnie do podwojenia\n");
        for (int a = 0; a < n; a++) {
            if (tab[a]>0) {
                System.out.print(tab[a] + ",");
                wynik+=2*tab[a];
            }
        }


        System.out.println("\nWynik to "+wynik);
    }
}

class Zadanie2_3{
    public static void main(String[] args) {
        int n = 8;
        int zera = 0;
        int dodatnie = 0;
        int ujemne= 0;
        int[] tab = new int[8];
        tab[0] = 2;
        tab[1] = 7;
        tab[2] = 1;
        tab[3] = 0;
        tab[4] = 3;
        tab[5] = 6;
        tab[6] = -1;
        tab[7] = 0;
        for (int a = 0; a < n; a++)
        {
            if (tab[a]>0) {
                dodatnie++;
                continue;
            }
            if (tab[a]<0) {
                ujemne++;
                continue;
            }
            zera++;
        }
        System.out.println("\nZer jest "+zera+"\nujemnych jest "+ujemne+"\nDodatnich jest "+dodatnie);
    }
}

class Zadanie2_4{
    public static void main(String[] args) {
        int n=5;
        int[] tab=new int[n];
        for(int i=0;i<n;)
        {
            tab[i]=++i;
        }
        int max=tab[0];
        int min=tab[0];
        for(int a = 0; a < tab.length; a++)
        {
            if (max<tab[a])
            {
                max=tab[a];
            }
            if (min>tab[a])
            {
                min=tab[a];
            }
        }
        System.out.println("Najmniejsza jest "+min+"\nNjawieksza jest "+max);
    }
}

class Zadanie2_5{
    public static void main(String[] args) {
        int wynik=0;
        int n=5;
        double[] tab=new double[n];
        for(int i=0;i<n;)
        {
            tab[i]=++i;
        }
        tab[3]=-1;
        for (int i = 0; i < tab.length-1; i++)
        {
            if(tab[i]>0&&tab[i+1]>0)
            {
                System.out.println("("+tab[i]+","+tab[i+1]+")");
                wynik++;
            }
        }
        System.out.println("Takich par jest: "+wynik);
    }
}






