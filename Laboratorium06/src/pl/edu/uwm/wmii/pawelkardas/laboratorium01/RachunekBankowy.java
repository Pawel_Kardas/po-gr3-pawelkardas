package pl.edu.uwm.wmii.pawelkardas.laboratorium01;

public class RachunekBankowy {


    public RachunekBankowy(double saldo) {
        this.saldo = saldo;
    }
    public void obliczMiesieczneOdsetki()
    {
        double tmp =(saldo*rocznaStopaProcentowa) / 12;
        saldo+=tmp;
    }

    public double getSaldo() {
        return saldo;
    }

    public static void setRocznaStopaProcentowa(double rocznaStopaProcentowa) {
        RachunekBankowy.rocznaStopaProcentowa = rocznaStopaProcentowa/100;
    }
    static double rocznaStopaProcentowa=0;
    private double saldo;
}
