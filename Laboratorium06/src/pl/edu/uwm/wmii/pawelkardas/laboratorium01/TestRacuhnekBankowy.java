package pl.edu.uwm.wmii.pawelkardas.laboratorium01;

class TestRachunekBankowy {
    public static void main(String[]args)
    {
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        RachunekBankowy.setRocznaStopaProcentowa(4);
        System.out.println("saver1: "+saver1.getSaldo());
        System.out.println("saver2: "+saver2.getSaldo());
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println();
        System.out.println("saver1: "+saver1.getSaldo());
        System.out.println("saver2: "+saver2.getSaldo());
        RachunekBankowy.setRocznaStopaProcentowa(5);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println();
        System.out.println("saver1: "+saver1.getSaldo());
        System.out.println("saver2: "+saver2.getSaldo());
    }
}
