package pl.edu.uwm.wmii.pawelkardas.laboratorium01;

import java.time.LocalDate;

public class TestPracownikVer0 {

    public static void main(String[] args) {
        Pracownik[] personel = new Pracownik[3];


        personel[0] = new Pracownik("Jan Kowalski", 75000, 2000, 12, 15);
        personel[1] = new Pracownik("Janusz Kowalski", 45000, 2001, 11, 14);
        personel[2] = new Pracownik("Janusz Kowalewski", 50000, 2002, 10, 13);


        for (Pracownik e : personel) {
            e.zwiekszPobory(20);
        }


        for (Pracownik e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();





        for (Pracownik e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();

    }
}

class Pracownik {

    public Pracownik(String nazwisko, double pobory, int year, int month, int day) {
        this.nazwisko = nazwisko;
        this.pobory = pobory;
        dataZatrudnienia =  LocalDate.of(year,month,day);

    }

    public String nazwisko() {
        return nazwisko;
    }

    public double pobory() {
        return pobory;
    }

    public LocalDate dataZatrudnienia() {

        return dataZatrudnienia;


    }

    public void zwiekszPobory(double procent) {
        double podwyżka = pobory * procent / 100;
        pobory += podwyżka;
    }

    private String nazwisko;
    private double pobory;
    private final LocalDate dataZatrudnienia;
}
