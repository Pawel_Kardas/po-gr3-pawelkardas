package pl.edu.uwm.wmii.pawelkardas.laboratorium01;

public class TestIntegerSet {
    public static void main(String[]args)
    {
        IntegerSet set1 = new IntegerSet();
        IntegerSet set2 = new IntegerSet();
        for(int i=1;i<100;i+=3)
        {
            set1.insertElement(i);
        }
        set1.deleteElement(1);
        for(int i=1;i<100;i+=6)
        {
            set2.insertElement(i);
        }
        System.out.println(set1);
        System.out.println(set2);

        IntegerSet unionset1set2 = IntegerSet.union(set1,set2);
        System.out.println(unionset1set2);
        IntegerSet intersectionset1set2 = IntegerSet.intersection(set1,set2);
        System.out.println(intersectionset1set2);
        IntegerSet set3 = new IntegerSet();
        for(int i=1;i<100;i+=3)
        {
            set3.insertElement(i);
        }
        System.out.println(set1.equals(set2));
        System.out.println(set1.equals(set3));
    }
}
