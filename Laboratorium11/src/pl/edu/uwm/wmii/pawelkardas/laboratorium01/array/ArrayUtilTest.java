package pl.edu.uwm.wmii.pawelkardas.laboratorium01.array;

import java.time.LocalDate;

public class ArrayUtilTest {
    public static void main(String[] args) {
        Integer[] tab = {64, 25, 12, 22, 11};
        Integer[] tab2 = {11, 12, 25, 22, 64};
        Integer[] tab3 = {11, 12, 22, 25, 64};

        LocalDate[] tabdaty = {LocalDate.parse("2000-01-02"), LocalDate.parse("2001-01-02"), LocalDate.parse("2003-01-02"), LocalDate.parse("2000-01-02"), LocalDate.parse("1999-01-02"), LocalDate.parse("2003-01-02")};
        LocalDate[] tabdaty2 = { LocalDate.parse("1999-01-02"),LocalDate.parse("2000-01-02"), LocalDate.parse("2003-01-02")};

        System.out.println(ArrayUtil.isSorted(tab));
        System.out.println(ArrayUtil.isSorted(tab2));
        System.out.println(ArrayUtil.isSorted(tab3));

        System.out.println(ArrayUtil.isSorted(tabdaty));
        System.out.println(ArrayUtil.isSorted(tabdaty2));

        System.out.println(ArrayUtil.binSearch(tab,64));
        System.out.println(ArrayUtil.binSearch(tab3,64));

        System.out.println(ArrayUtil.binSearch(tabdaty,LocalDate.parse("1999-01-02")));
        System.out.println(ArrayUtil.binSearch(tabdaty2,LocalDate.parse("1999-01-22")));

        ArrayUtil.mergeSort(tab,tab.length);
        for(int x:tab)
        {
            System.out.println(x);
        }
        ArrayUtil.mergeSort(tabdaty,tabdaty.length);
        for(LocalDate x:tabdaty)
        {
            System.out.println(x);
        }

        System.out.println("Selection sort");
        Integer []select = {1,2,5,2,3,6,1,7,2,3,5,9};
        for(int x:select)
        {
            System.out.printf("%d ",x);
        }
        System.out.println();
        ArrayUtil.selectionSort(select);
        for(int x:select)
        {
            System.out.printf("%d ",x);
        }

    }
}