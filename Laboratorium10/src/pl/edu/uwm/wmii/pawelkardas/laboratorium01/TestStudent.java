package pl.edu.uwm.wmii.pawelkardas.laboratorium01;

import pl.edu.uwm.wmii.pawelkardas.laboratorium01.imiajd.kardas.Student;

import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String [] args) throws CloneNotSupportedException {
        Student ja = new Student("Kardas","2000-02-05",4.5);
        Student kowalski = new Student("Kowalski","2000-02-05",4.5);
        Student Kardi = new Student("Kardi","2000-02-05",4.6);
        Student kowalski2 = new Student("Kowalski","1996-02-05",4.5);
        Student Janusz = new Student("Janusz","1990-03-05",2.2);
        Student Janusz2 = new Student("Janusz2","1990-03-05",2.2);
        ArrayList<Student>tab = new ArrayList<>();
        tab.add(ja);tab.add(Kardi);tab.add(kowalski);tab.add(kowalski2);tab.add(Janusz);
        System.out.println(kowalski.equals(kowalski2));
        System.out.println(Janusz.equals(Janusz2));
        System.out.println(tab);
        Collections.sort(tab);
        System.out.println(tab);

        Student Januszclone = Janusz.clone();
        System.out.println(Januszclone);
        Januszclone.setSredniaOcen(2.3);
        System.out.println(Janusz);
        System.out.println(Januszclone);


    }
}
