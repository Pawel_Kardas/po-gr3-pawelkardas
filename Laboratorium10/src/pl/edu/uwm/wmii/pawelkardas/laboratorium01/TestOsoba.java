package pl.edu.uwm.wmii.pawelkardas.laboratorium01.imiajd.kardas;

import java.util.ArrayList;

public class TestOsoba {
    public static void main(String[]args) throws CloneNotSupportedException {
        Osoba kowalski = new Osoba("Kardas","1996-02-05");
        Osoba kowalski2 = new Osoba("Kowalski","2000-01-31");
        Osoba kowalski4 = new Osoba("Kowalski","2000-02-01");
        Osoba kowalski3 = new Osoba("Lewandowski","2000-01-01");
        Osoba ja = new Osoba("Kardas","2000-02-05");
        ArrayList<Osoba> grupa = new ArrayList<>();
        grupa.add(kowalski);grupa.add(kowalski2);grupa.add(kowalski4);grupa.add(kowalski3);grupa.add(ja);
        System.out.println(kowalski.compareTo(kowalski2));
        System.out.println(kowalski4.compareTo(kowalski2));
        System.out.println(ja.equals(kowalski4));
        System.out.println(grupa);
        grupa.sort(Osoba::compareTo);

        System.out.println(grupa);
        Osoba kowalski4clone = kowalski4.clone();
        System.out.println(kowalski4clone);
        kowalski4clone.setNazwisko("Kowalski4");
        System.out.println(kowalski4clone);
        System.out.println(kowalski4);
    }
}
